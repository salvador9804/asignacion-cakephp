<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class Usuario extends Entity
{
    protected function _setContrasenia($psswd)
    {
        if (strlen($psswd)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($psswd);
        }
    }
}