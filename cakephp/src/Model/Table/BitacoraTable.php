<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use Cake\ORM\Table;


class BitacoraTable extends Table {
    public function initialize(array $config) {
        parent::initialize($config);
        //A cuál corresponde en la base de datos
        $this->setTable('bitacora');
        //Llave primaria
        $this->setPrimaryKey('idregistro');
        
       $this->belongsTo('Usuario', [
        'className' => 'Usuario',
        'foreignKey' => 'idusuario'
       ]);
       
       $this->belongsTo('Accion', [
        'className' => 'Accion',
        'foreignKey' => 'idaccion'
       ]);       
    }
}

