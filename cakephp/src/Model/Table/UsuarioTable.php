<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;
/**
 * CakePHP UsuarioTable
 * @author root
 */
class UsuarioTable extends Table {
    public function initialize(array $config) {
        parent::initialize($config);
        //A cuál corresponde en la base de datos
        $this->setTable('usuario');
        //Llave primaria
        $this->setPrimaryKey('idusuario');
       
        $this->hasMany('Bitacora', [
        'className' => 'Bitacora',
        'foreignKey' => 'idusuario'
       ]);
    }
    
    public function validationDefault(Validator $validator){
        
        $validator
            ->requirePresence('correo')
            ->notEmpty('correo', 'Se requiere de un correo electrónico.');
        
                $validator
            ->requirePresence('apepaterno')
            ->notEmpty('apepaterno', 'Se requiere de un apellido por lo menos.');
                
        $validator
            ->requirePresence('nombre')
            ->notEmpty('nombre', 'Se requiere del nombre del usuario.');
        
        $validator
            ->requirePresence('estado')
            ->notEmpty('estado', 'Debes señalar el estado inicial del usuario.');

        $validator
            ->requirePresence('rol')
            ->notEmpty('rol', 'Debes señalar si será administrador o usuario.');        
        
        $validator->regex('correo', '/^([A-Za-z0-9-_]+)@([A-Za-z0-9-_]+)\.([A-Za-z0-9-_]+)([A-Za-z0-9-_\.])*$/', 'El correo electrónico no es válido.');
        //'^[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'
        
        return $validator;
          
    }

    public function buildRules(RulesChecker $reglas){
        
        $reglas->add($reglas->isUnique(
            ['correo'], 'Este correo ya se encuentra registrado.'
        ));
        
        return $reglas;
    }
}