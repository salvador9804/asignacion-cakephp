<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use Cake\ORM\Table;
/**
 * CakePHP AccionTable
 * @author root
 */
class AccionTable extends Table {
    public function initialize(array $config) {
        parent::initialize($config);
        //A cuál corresponde en la base de datos
        $this->setTable('accion');
        //Llave primaria
        $this->setPrimaryKey('idaccion');
        
       $this->hasMany('Bitacora', [
        'className' => 'Bitacora',
        'foreignKey' => 'idaccion'
       ]);
    }
}
