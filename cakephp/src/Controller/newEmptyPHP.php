<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

class UsuarioController extends AppController{
    
    public $paginate = [
            'limit' => 10,
            'order' => ['Usuario.apepaterno' => 'asc']
        ];

    public function initialize(){
            parent::initialize();
            $this->loadComponent('Paginator');
    }

    public function index(){
            $query = $this->Usuario->find();
            $data = $this->paginate($query);
            $this->set(compact('data')); //Envia todos los registros hallados en la tabla a la vista (Usuario\index.ctp).
    }
    
    public function login(){
        if ($this->request->is('post')) {
            
            $usuario = $this->Auth->identify();
            $estado = $this->Usuario->find()
                    ->select(['estado'])
                    ->where(['correo =' => $this->request->getParam('correo')]); //Se busca el estado del usuario que busca ingresar para saber si esta habilitado o deshabilitado

            if($estado->estado == 0){
                $this->Flash->error('Estás deshabilitado');
            } else{
                if ($usuario){
                    $this->Auth->setUser($usuario);
                    return $this->redirect($this->Auth->redirectUrl());
                } else{
                    $this->Flash->error('Correo o contraseña incorrectos');
                }
            }          
        }
    }
    
    public function nuevo(){
        
        $registroBitacora = TableRegistry::get('Bitacora');
//        $registro = $registroBitacora->newEntity();
     
        //A idusuario se le asigna el id del usuario que está almacenado en sesión
//        $query = $registroBitacora->query();
//        $query->insert(['title', 'body', 'idusuario', 'idaccion', 'fecha', 'hora'])
//          ->values([
//            'idusuario' => $this->Auth->user('idusuario'),
//            'idaccion' => 1,
//            'fecha' => date("Y-m-d"),
//            'hora' => date("H:i:s")  
//          ])
//         ->execute();
        
        $registro->idusuario = $this->Auth->usuario('idusuario');
        $registro->idaccion = 1;
        $registro->fecha = date("Y-m-d");
        $registro->hora = date("H:i:s");
        
        
        //Se genera una nueva entidad (nuevo registro).
        $usuario = $this->Usuario->newEntity();
        
        if ($this->request->is('post')) {
            
            //Se hace una validaciòn de los datos obtenidos en getData antes de pasarlos a $usuario, donde está la nueva entidad.
            $usuario = $this->Usuario->patchEntity($usuario, $this->request->getData()); 

            if ($this->Usuario->save($usuario) && $registroBitacora->save($registro)) {
                $this->Flash->success(__('Éxito en la creación del usuario.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Hubo un problema en la creación del usuario.'));
        }
        $this->set('usuario', $usuario);
    }
    
    public function editar($id){
        
        $registroBitacora = TableRegistry::get('Bitacora');
        $registro = $registroBitacora->newEntity();
     
        $registro->idusuario = $this->Auth->usuario('idusuario');
        $registro->idaccion = 2;
        $registro->fecha = date("Y-m-d");
        $registro->hora = date("H:i:s");
        
        
        //Obtiene el registro que coincida con el id enviado como parametro.
        $usuario = $this->Usuario->get($id);
        
        if ($this->request->is(['post', 'put'])) {
            
            //Se hace una validaciòn de los datos obtenidos en getData antes de pasarlos a $usuario, donde está el registro obtenido previamente.
            $this->Usuario->patchEntity($usuario, $this->request->getData());
            
            if ($this->Usuario->save($usuario) && $registroBitacora->save($registro)) {
                $this->Flash->success(__('Éxito en la actualización de los datos del usuario.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Hubo un problema en la actualizaciòn del usuario.'));
        }

    $this->set('usuario', $usuario);
        
    }
    
    public function eliminar($id){
        
        //Se crea una nueva entidad para guardar los datos de la eliminacion en bitacora
        $registroBitacora = TableRegistry::get('Bitacora');
        $registro = $registroBitacora->newEntity();
     
        $registro->idusuario = $this->Auth->usuario('idusuario');
        $registro->idaccion = 3;
        $registro->fecha = date("Y-m-d");
        $registro->hora = date("H:i:s");

        
        $usuario = $this->Usuario->get($id); //Obtiene el registro de la base de datos a través del id recibido como parámetro
        
        if($this->Usuario->delete($usuario) && $registroBitacora->save($registro)){
  
            $this->Flash->success(__('Usuario borrado con éxito.'));
                 
        }else{  
            
            $this->Flash->error(__('Hubo un error en la eliminación del usuario.'));
            
        }
        return $this->redirect(['action'=>'index']);
    }
}
?>