
<div class="container">
    
    
    <h2 class="text-center">Bienvenido - Login</h2>
    <hr>
    <?= $this->Form->create() ?>
    <div class="form-group">
    <?php
        echo $this->Form->control('correo', ['type' => 'email', 'label' => 'Correo electrónico', 
            'placeholder' => 'Ingresa tu correo electronico', 'class' => 'form-control']);
    ?>    
    </div>

    <div class="form-group">
    <?php
        echo $this->Form->control('contrasenia', ['type' => 'password', 'label' => 'Contraseña', 
            'placeholder' => 'Ingresa tu contraseña', 'class' => 'form-control']);
    ?>    
    </div>
    
    <div class="text-center">
        <?= $this->Form->button(__('Entrar'), ['class' => 'btn btn-success']) ?>
    </div>
    
    <?= $this->Form->end() ?>
      
</div>

