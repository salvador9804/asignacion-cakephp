<!--<title>Agregar Usuarios</title>-->
<div class="container">
    
    <br>
    <div class="col-md-12">
        <?= $this->Html->link('Volver',
            ['action' => 'index'],
            ['class' => 'btn btn-primary btn-sm']
        ); ?>
    </div>
    <br>
    
    <h2 class="text-center">Agregar Usuario</h2>
    
    <fieldset>
    <?php
        echo $this->Form->create($usuario);
        $psswd = substr(md5(microtime()), 1, 8);
        echo $this->Form->control('contrasenia', ['type' => 'hidden', 'value' => $psswd]);
        var_dump($psswd);
    ?>
    
    <div class="form-group">
    <?php
        echo $this->Form->control('correo', ['type' => 'email', 'label' => 'Correo electrónico', 
            'placeholder' => 'Ingresa tu correo electronico', 'class' => 'form-control']);
    ?>    
    </div>

    <div class="form-group">
    <?php    
        echo $this->Form->control('apepaterno', ['label' => 'Apellido Paterno', 
            'placeholder' => 'Ingresa tu apellido', 'class' => 'form-control']);
    ?>    
    </div>
        
    <div class="form-group">
    <?php
        echo $this->Form->control('apematerno', ['label' => 'Apellido Materno', 
            'placeholder' => 'Ingresa un segundo apellido (opcional)', 'class' => 'form-control']);
    ?>    
    </div>    
        
    <div class="form-group">
    <?php
        echo $this->Form->control('nombre', ['label' => 'Nombre', 
            'placeholder' => 'Ingresa tu nombre', 'class' => 'form-control']);
    ?>    
    </div>
        
        <label class="radio-inline">Estado<br>
       <?= $this->Form->radio('estado', ['Deshabilitado','Habilitado']); ?>
        </label>
        <br>
        
        <label class="radio-inline">Rol<br>
       <?= $this->Form->radio('rol', ['Usuario','Administrador']); ?>
        </label>
    </fieldset>
    
    <?php
    //faltan los radio button para el estado y el rol del usuario

        echo $this->Form->button(__('Guardar'), ['class' => 'btn btn-success']);
        echo $this->Form->button('Refrescar', ['type' => 'reset','class' => 'btn btn-danger']);
        echo $this->Form->end();
    ?>
</div>
