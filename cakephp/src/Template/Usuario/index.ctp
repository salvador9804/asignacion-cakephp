<br><div class="text-left">
    <?= $this->Html->link('Cerrar sesión',
        ['action' => 'logout'],
        ['class' => 'btn btn-danger']
    ); ?>
</div>

<br><h3 class="text-center">Listado de Usuarios</h3><br>

    <div class="row">

        <div class="col-md-12 text-right">
                <?= $this->Html->link('Agregar Usuario',
                    ['action' => 'nuevo'],
                    ['class' => 'btn btn-success']
            ); ?>
        </div>
    </div>
    <br>
    <table class="table table-stripped">
        <thead class="thead-dark">
            <tr>
                <th><?php echo $this->Paginator->sort('Usuario.apepaterno', 'Apellido Paterno'); ?></th>
                <th><?php echo $this->Paginator->sort('Usuario.apematerno', 'Apellido Materno'); ?></th>
                <th><?php echo $this->Paginator->sort('Usuario.nombre', 'Nombre'); ?></th>
                <th><?php echo $this->Paginator->sort('Usuario.correo', 'Email'); ?></th>
                <th>Rol</th>
                <th>Estado</th>
                <th colspan="2">Opciones</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($data as $usuario){
                echo "<tr>";
                    echo "<td> {$usuario['apepaterno']} </td>";
                    echo "<td> {$usuario['apematerno']} </td>";
                    echo "<td> {$usuario['nombre']} </td>";
                    echo "<td> {$usuario['correo']} </td>";
                    if ($usuario['rol'] == 1){
                        echo "<td> Administrador </td>";
                    } else{
                        echo "<td> Usuario </td>";
                    }
                    
                    if ($usuario['estado'] == 1){
                        echo "<td> Habilitado </td>";
                    } else{
                        echo "<td> Deshabilitado </td>";
                    }
                    
                    echo "<td>";
                    echo $this->Html->link(__('Editar'),
                         ['action' => 'editar', $usuario['idusuario']],
                         ['class' => 'btn btn-sm btn-primary']);
                    echo $this->Html->link(__('Eliminar'),
                         ['action' => 'eliminar', $usuario['idusuario']],
                         ['class' => 'btn btn-sm btn-danger', 'confirm' => '¿Estás seguro de que quieres eliminar este usuario?']);
                    echo "</td>";
                    echo "</tr>";
            }
            ?>
        </tbody>

    </table>
    <?php 
        echo $this->element('paginacion');
    ?>

