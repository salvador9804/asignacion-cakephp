<!--<title>Editar Usuarios</title>-->
<div class="container">
    
    <br>
    <div class="col-md-12">
        <?= $this->Html->link('Volver',
            ['action' => 'index'],
            ['class' => 'btn btn-primary btn-sm']
        ); ?>
    </div>
    <br>
    
    <h2 class="text-center">Editar Usuario</h2>
    
    <?= $this->Form->create($usuario); ?>

    <fieldset>

    <div class="form-group">
    <?php    
        echo $this->Form->control('apepaterno', ['label' => 'Apellido Paterno', 
            'placeholder' => 'Ingresa tu apellido', 'class' => 'form-control']);
    ?>    
    </div>
        
    <div class="form-group">
    <?php
        echo $this->Form->control('apematerno', ['label' => 'Apellido Materno', 
            'placeholder' => 'Ingresa un segundo apellido (opcional)', 'class' => 'form-control']);
    ?>    
    </div>    
        
    <div class="form-group">
    <?php
        echo $this->Form->control('nombre', ['label' => 'Nombre', 
            'placeholder' => 'Ingresa tu nombre', 'class' => 'form-control']);
    ?>    
    </div>
        
        <label class="radio-inline">Estado<br>
       <?= $this->Form->radio('estado', ['Deshabilitado','Habilitado']); ?>
        </label>
        <br>
        
        <label class="radio-inline">Rol<br>
       <?= $this->Form->radio('rol', ['Usuario','Administrador']); ?>
        </label>
    </fieldset>
    
    <?php
    //faltan los radio button para el estado y el rol del usuario

        echo $this->Form->button(__('Guardar'), ['class' => 'btn btn-success']);
        echo $this->Form->button('Refrescar', ['type' => 'reset','class' => 'btn btn-danger']);
        echo $this->Form->end();
    ?>
</div>

