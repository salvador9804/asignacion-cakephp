<?php
// Change a template
$this->Paginator->setTemplates([
    'nextActive' => '<a class="btn btn-outline-primary btn-sm" rel="next" href="{{url}}">{{text}}</a>',
    'nextDisabled' => '<a class="btn btn-outline-dark btn-sm disabled" href="" onclick="return false;">{{text}}</a>',
    'prevActive' => '<a class="btn btn-outline-primary btn-sm" rel="prev" href="{{url}}">{{text}}</a>',
    'prevDisabled' => '<a class="btn btn-outline-dark btn-sm disabled" onclick="return false;">{{text}}</a>',
    'current' => '<a class="btn btn-outline-dark btn-sm disabled" href="">{{text}}</a>&nbsp;',
    'number' => '<a class="btn btn-outline-primary btn-sm" href="{{url}}">{{text}}</a>&nbsp;'
]);
?>
<div class="row">
    <div class="col-sm-6 offset-sm-3 text-center">
        <?= $this->Paginator->counter('Página {{page}} de {{pages}}'); ?>
    </div>
    <div class="col-sm-6 offset-sm-3 text-center">
        <?= $this->Paginator->prev('<i class="fas fa-caret-square-left"></i> Anterior', ['escape'=> false]); ?>
        <?= $this->Paginator->numbers(); ?>
        <?= $this->Paginator->next('Siguiente <i class="fas fa-caret-square-right"></i>', ['escape'=> false]); ?>
    </div>
</div>
<div class="clear">&nbsp;</div>