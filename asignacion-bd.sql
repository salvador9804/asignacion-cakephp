CREATE DATABASE prueba;

USE prueba;

CREATE SEQUENCE seq_usuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    NO CYCLE;

CREATE SEQUENCE seq_bitacora
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    NO CYCLE;

CREATE TABLE public.usuario(
    idusuario numeric(2,0) NOT NULL DEFAULT nextval('seq_usuario'),
    correo character varying(30) NOT NULL,
    apepaterno character varying(30) NOT NULL,
    apematerno character varying(30),
    nombre character varying(30) NOT NULL,
    estado numeric(1,0) NOT NULL,
    rol numeric(1,0) NOT NULL,
    contrasenia character varying(50) NOT NULL,
    CONSTRAINT pkusuario PRIMARY KEY (idusuario),
    CONSTRAINT uqusuariocorreo UNIQUE (correo)
)

CREATE TABLE accion(
    idaccion numeric(1,0),
    accion numeric(2,0) NOT NULL,
    CONSTRAINT pkaccion PRIMARY KEY (idaccion)
)

CREATE TABLE bitacora(
    idregistro numeric(4,0) DEFAULT nextval('seq_bitacora'),
    idusuario numeric(2,0) NOT NULL,
    idaccion numeric(1,0) NOT NULL,
    fecha date NOT NULL,
    hora date NOT NULL,
    CONSTRAINT pkbitacora PRIMARY KEY (idregistro),
    CONSTRAINT fkbitacora_usuario FOREIGN KEY (idusuario) REFERENCES usuario(idusuario) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkbitacora_accion FOREIGN KEY (idaccion) REFERENCES accion(idaccion) ON DELETE RESTRICT ON UPDATE CASCADE
)